package service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import dao.PersonDAO;
import model.Person;

@Service
public class PersonService {

	private final PersonDAO personDao;
	
	@Autowired
	public PersonService(@Qualifier("fakeDao") PersonDAO personDao) {
		this.personDao = personDao;
	}

	public int addPerson(Person person) {
		return personDao.insertPerson( person);
	}
	
	public List<Person> getAllPeople() {
		return personDao.selectAllPeople();
	}
}
